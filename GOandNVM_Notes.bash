sudo apt-get update
sudo apt-get -y upgrade

curl -O https://storage.googleapis.com/golang/go1.11.2.linux-amd64.tar.gz

tar -xvf go1.11.2.linux-amd64.tar.gz

sudo mv go /usr/local


#GO VARIABLES
GOPATH is the workspace in which you want to create the GO Project. 
export GOPATH=/home/ubuntu/GoWorkspace
GOROOT is the directory where go will be installed
export GOROOT=/usr/local/go
export PATH=$PATH:$GOROOT/bin
#END GO VARIABLES

#################################################################
# Install NVM 
#################################################################

wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash

source ~/.profile

nvm --version

nvm ls-remote

